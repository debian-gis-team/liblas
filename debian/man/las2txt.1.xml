<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE refentry PUBLIC "-//OASIS//DTD DocBook XML V4.4//EN" "http://www.oasis-open.org/docbook/xml/4.4/docbookx.dtd">
<refentry id='las2txt'>

  <refmeta>
    <refentrytitle>las2txt</refentrytitle>
    <manvolnum>1</manvolnum>
  </refmeta>

  <refnamediv>
    <refname>las2txt</refname>
    <refpurpose>output ASRPS LAS files into ASCII text</refpurpose>
  </refnamediv>

  <refsynopsisdiv id='synopsis'>
    <cmdsynopsis>
      <command>las2txt</command>
      <group>
        <arg choice='plain'><option>-h</option></arg>
        <arg choice='plain'>
          <option>-i</option> <arg choice='plain'><replaceable>&lt;infile&gt;</replaceable></arg>
          <option>-o</option> <arg choice='plain'><replaceable>&lt;outfile&gt;</replaceable></arg>
          <option>-t</option> <arg choice='plain'><replaceable>&lt;format&gt;</replaceable></arg>
        </arg>
        <arg choice='plain'><option>-formats</option></arg>
      </group>
    </cmdsynopsis>
  </refsynopsisdiv>

  <refsect1 id='description'>
    <title>DESCRIPTION</title>
    <para>
      <command>las2txt</command> allows you to output ASRPS LAS files into
      ASCII text.
    </para>
    <para>
      The '<option>-parse txyz</option>' flag specifies how to format each line
      of the ASCII file.
      For example, '<literal>txyzia</literal>' means that the first number of
      each line should be the gpstime,
      the next three numbers should be the x, y, and z coordinate,
      the next number should be the intensity and
      the next number should be the scan angle.
    </para>
    <para>
      Supported entries
      <itemizedlist>
        <listitem>
          <para>a - scan angle</para>
        </listitem>
        <listitem>
          <para>i - intensity</para>
        </listitem>
        <listitem>
          <para>n - number of returns for given pulse</para>
        </listitem>
        <listitem>
          <para>r - number of this return</para>
        </listitem>
        <listitem>
          <para>c - classification</para>
        </listitem>
        <listitem>
          <para>u - user data</para>
        </listitem>
        <listitem>
          <para>p - point source ID</para>
        </listitem>
        <listitem>
          <para>e - edge of flight line flag</para>
        </listitem>
        <listitem>
          <para>d - direction of scan flag </para>
        </listitem>
        <listitem>
          <para>R - red channel of RGB color</para>
        </listitem>
        <listitem>
          <para>G - green channel of RGB color</para>
        </listitem>
        <listitem>
          <para>B - blue channel of RGB color</para>
        </listitem>
        <listitem>
          <para>M - vertex index number</para>
        </listitem>
      </itemizedlist>
    </para>

    <para>
      Separators
    </para>
    <para>
      The '<option>-sep space</option>' flag specifies what separator to use.
      The default is a space.
      The following list is valid for separator characters (case sensitive):
      <itemizedlist>
        <listitem>
          <para>tab</para>
        </listitem>
      </itemizedlist>
      <itemizedlist>
        <listitem>
          <para>comma</para>
        </listitem>
      </itemizedlist>
      <itemizedlist>
        <listitem>
          <para>colon</para>
        </listitem>
      </itemizedlist>
      <itemizedlist>
        <listitem>
          <para>hyphen</para>
        </listitem>
      </itemizedlist>
      <itemizedlist>
        <listitem>
          <para>dot</para>
        </listitem>
      </itemizedlist>
      <itemizedlist>
        <listitem>
          <para>semicolon</para>
        </listitem>
      </itemizedlist>
    </para>

    <para>
      Header Separators
    </para>
    <para>
      The '<option>-header pound</option>' flag results in the header
      information being printed at the beginning of the ASCII file in
      form of a comment that starts with the special character '#'.
      Other possible special characters include (case sensitive):
      <itemizedlist>
        <listitem>
          <para>percent</para>
        </listitem>
        <listitem>
          <para>dollar</para>
        </listitem>
        <listitem>
          <para>semicolon</para>
        </listitem>
        <listitem>
          <para>comma</para>
        </listitem>
        <listitem>
          <para>star</para>
        </listitem>
        <listitem>
          <para>colon</para>
        </listitem>
        <listitem>
          <para>semicolon</para>
        </listitem>
      </itemizedlist>
    </para>
  </refsect1>

  <refsect1 id='options'>
    <title>OPTIONS</title>

    las2txt options:
    <variablelist>

      <varlistentry>
        <term><option>-h</option></term>
        <term><option>--help</option></term>
        <listitem>
          <para>
            produce help message
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-i</option> <replaceable>arg</replaceable></term>
        <term><option>--input</option> <replaceable>arg</replaceable></term>
        <listitem>
          <para>
            input LAS file.
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-o</option> <replaceable>arg</replaceable></term>
        <term><option>--output</option> <replaceable>arg</replaceable></term>
        <listitem>
          <para>
            output text file.
            Use 'stdout' if you want it written to the standard output stream
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>--parse</option> <replaceable>arg</replaceable></term>
        <listitem>
          <para>
            The '<option>--parse txyz</option>' flag specifies how to format
            each line of the ASCII file.
          </para>
          <para>
            For example, '<literal>txyzia</literal>' means that the first
            number of each line should be the gpstime,
            the next three numbers should be the x, y, and z coordinate,
            the next number should be the intensity and
            the next number should be the scan angle.
          </para>
          <para>
            The supported entries are:
            <itemizedlist>
              <listitem>
                <para>x - x coordinate as a double</para>
              </listitem>
              <listitem>
                <para>y - y coordinate as a double</para>
              </listitem>
              <listitem>
                <para>z - z coordinate as a double</para>
              </listitem>
              <listitem>
                <para>X - x coordinate as unscaled integer</para>
              </listitem>
              <listitem>
                <para>Y - y coordinate as unscaled integer</para>
              </listitem>
              <listitem>
                <para>Z - z coordinate as unscaled integer</para>
              </listitem>
              <listitem>
                <para>a - scan angle</para>
              </listitem>
              <listitem>
                <para>i - intensity</para>
              </listitem>
              <listitem>
                <para>n - number of returns for given pulse</para>
              </listitem>
              <listitem>
                <para>r - number of this return</para>
              </listitem>
              <listitem>
                <para>c - classification number</para>
              </listitem>
              <listitem>
                <para>C - classification name</para>
              </listitem>
              <listitem>
                <para>u - user data</para>
              </listitem>
              <listitem>
                <para>p - point source ID</para>
              </listitem>
              <listitem>
                <para>e - edge of flight line</para>
              </listitem>
              <listitem>
                <para>d - direction of scan flag</para>
              </listitem>
              <listitem>
                <para>R - red channel of RGB color</para>
              </listitem>
              <listitem>
                <para>G - green channel of RGB color</para>
              </listitem>
              <listitem>
                <para>B - blue channel of RGB color</para>
              </listitem>
              <listitem>
                <para>M - vertex index number</para>
              </listitem>
            </itemizedlist>
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>--precision</option> <replaceable>arg</replaceable></term>
        <listitem>
          <para>
            The number of decimal places to use for x,y,z,[t] output.  
            <option>--precision 7 7 3</option>
            <option>--precision 3 3 4 6</option>
            If you don't specify any precision, las2txt uses the 
            implicit values defined by the header's scale value 
            (and a precision of 8 is used for any time values.)
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>--delimiter</option> <replaceable>arg</replaceable></term>
        <listitem>
          <para>
            The character to use for delimiting fields in the  output.
            <option>--delimiter ","</option>
            <option>--delimiter ""</option>
            <option>--delimiter " "</option>
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>--labels</option></term>
        <listitem>
          <para>
            Print row of header labels
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>--header</option></term>
        <listitem>
          <para>
            Print header information
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-v</option></term>
        <term><option>--verbose</option></term>
        <listitem>
          <para>
            Verbose message output
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>--xml</option></term>
        <listitem>
          <para>
            Output as XML.
            No formatting given by <option>--parse</option>
            is respected in this case.
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>--stdout</option></term>
        <listitem>
          <para>
            Output data to stdout
          </para>
        </listitem>
      </varlistentry>

    </variablelist>

    Filtering options:
    <variablelist>

      <varlistentry>
        <term><option>-e</option> <replaceable>arg</replaceable></term>
        <term><option>--extent</option> <replaceable>arg</replaceable></term>
        <listitem>
          <para>
            Extent window that points must fall within to keep.
            Use a comma-separated or quoted, space-separated list,
            for example, 
            <option>-e minx, miny, maxx, maxy</option>
            or 
            <option>-e minx, miny, minz, maxx, maxy, maxz</option>
            <option>-e "minx miny minz maxx maxy maxz"</option>
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>--minx</option> <replaceable>arg</replaceable></term>
        <listitem>
          <para>
            Extent must be greater than or equal to minx to be kept. 
            <option>--minx 1234.0</option>
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>--miny</option> <replaceable>arg</replaceable></term>
        <listitem>
          <para>
            Extent must be greater than or equal to miny to be kept. 
            <option>--miny 5678.0</option>
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>--minz</option> <replaceable>arg</replaceable></term>
        <listitem>
          <para>
            Extent must be greater than or equal to minz to be kept.
            If maxx and maxy are set but not minz *and maxz,
            all z values are kept.
            <option>--minz 0.0</option>
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>--maxx</option> <replaceable>arg</replaceable></term>
        <listitem>
          <para>
            Extent must be less than or equal to maxx to be kept. 
            <option>--maxx 1234.0</option>
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>--maxy</option> <replaceable>arg</replaceable></term>
        <listitem>
          <para>
            Extent must be less than or equal to maxy to be kept. 
            <option>--maxy 5678.0</option>
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>--maxz</option> <replaceable>arg</replaceable></term>
        <listitem>
          <para>
            Extent must be less than or equal to maxz to be kept. 
            If maxx and maxy are set but not maxz *and minz, all z
            values are kept. 
            <option>--maxz 10.0</option>
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-t</option>  <replaceable>arg</replaceable></term>
        <term><option>--thin</option> <replaceable>arg</replaceable> (=0)</term>
        <listitem>
          <para>
            Simple decimation-style thinning.
            Thin the file by removing every t'th point from the file.
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>--last-return-only</option></term>
        <listitem>
          <para>
            Keep last returns
            (cannot be used with <option>--first-return-only</option>)
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>--first-return-only</option></term>
        <listitem>
          <para>
            Keep first returns
            (cannot be used with <option>--last-return-only</option>)
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>--keep-returns</option> <replaceable>arg</replaceable></term>
        <listitem>
          <para>
            A list of return numbers to keep in the output file: 
            <option>--keep-returns 1 2 3</option>
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>--drop-returns</option> <replaceable>arg</replaceable></term>
        <listitem>
          <para>
            Return numbers to drop.
            For example, <option>--drop-returns 2 3 4 5</option>
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>--valid_only</option></term>
        <listitem>
          <para>
            Keep only valid points
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>--keep-classes</option> <replaceable>arg</replaceable></term>
        <listitem>
          <para>
            A list of classifications to keep:
            <option>--keep-classes 2 4 12</option>
            <option>--keep-classes 2</option>
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>--drop-classes</option> <replaceable>arg</replaceable></term>
        <listitem>
          <para>
            A comma-separated list of classifications to drop:
            <option>--drop-classes 1,7,8</option>
            <option>--drop-classes 2</option>
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>--keep-intensity</option> <replaceable>arg</replaceable></term>
        <listitem>
          <para>
            Range in which to keep intensity.
            The following expression types are supported:  
            <option>--keep-intensity 0-100</option>
            <option>--keep-intensity &lt;200</option>
            <option>--keep-intensity &gt;400</option>
            <option>--keep-intensity &gt;=200</option>
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>--drop-intensity</option> <replaceable>arg</replaceable></term>
        <listitem>
          <para>
            Range in which to drop intensity.
            The following expression types are supported:  
            <option>--drop-intensity &lt;200</option>
            <option>--drop-intensity &gt;400</option>
            <option>--drop-intensity &gt;=200</option>
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>--keep-time</option> <replaceable>arg</replaceable></term>
        <listitem>
          <para>
            Range in which to keep time.
            The following expression types are supported:  
            <option>--keep-time 413665.2336-414092.8462</option>
            <option>--keep-time &lt;414094.8462</option>
            <option>--keep-time &gt;413665.2336</option>
            <option>--keep-time &gt;=413665.2336</option>
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>--drop-time</option> <replaceable>arg</replaceable></term>
        <listitem>
          <para>
            Range in which to drop time.
            The following expression types are supported:  
            <option>--drop-time &lt;413666.2336</option>
            <option>--drop-time &gt;413665.2336</option>
            <option>--drop-time &gt;=413665.2336</option>
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>--keep-scan-angle</option> <replaceable>arg</replaceable></term>
        <listitem>
          <para>
            Range in which to keep scan angle.
            The following expression types are supported:  
            <option>--keep-scan-angle 0-100</option>
            <option>--keep-scan-angle &lt;100</option>
            <option>--keep-scan-angle &lt;=100</option>
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>--drop-scan-angle</option> <replaceable>arg</replaceable></term>
        <listitem>
          <para>
            Range in which to drop scan angle.
            The following expression types are supported:  
            <option>--drop-scan-angle &lt;30</option>
            <option>--drop-scan-angle &gt;100</option>
            <option>--drop-scan-angle &gt;=100</option>
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>--keep-color</option> <replaceable>arg</replaceable></term>
        <listitem>
          <para>
            Range in which to keep colors.
            Define colors as two 3-tuples (R,G,B-R,G,B):  
            <option>--keep-color '0,0,0-125,125,125'</option>
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>--drop-color</option> <replaceable>arg</replaceable></term>
        <listitem>
          <para>
            Range in which to drop colors.
            Define colors as two 3-tuples (R,G,B-R,G,B):  
            <option>--drop-color '255,255,255-65536,65536,65536'</option>
          </para>
        </listitem>
      </varlistentry>

    </variablelist>

    For more information, see the full documentation for lasinfo at:
    <ulink url="http://liblas.org/utilities/las2txt.html">http://liblas.org/utilities/las2txt.html</ulink>

  </refsect1>

  <refsect1 id='example'>
    <title>EXAMPLE</title>

    <example>
      <title>Simple conversion to text file</title>

      <screen>
$ las2txt -i lidar.las -o lidar.txt -parse xyz
      </screen>

      converts LAS file to ASCII and places the x, y, and z coordinate
      of each point at the 1st, 2nd, and 3rd entry of each line.
      The entries are separated by a space.
    </example>

    <example>
      <title>Specifying a separator</title>

      <screen>
$ las2txt -i lidar.las -o lidar.txt -parse txyzr -sep komma
      </screen>

      converts LAS file to ASCII and places the gps_time as the first
      entry, the x, y, and z coordinates at the 2nd, 3rd, and 4th entry
      and the number of the return as the 5th entry of each line.
      The entries are separated by a komma.
     </example>

    <example>
      <title>Altering the header separator</title>

      <screen>
$ las2txt -i lidar.las -o lidar.txt -parse xyzia -sep semicolon -header pound
      </screen>

      converts LAS file to ASCII and places the x, y, and z coordinate
      at the 1st, 2nd, and 3rd entry, the intensity at the 4th and the
      scan angle as the 5th entry of each line.
      The entries are separated by a semicolon.
      At the beginning of the file we print the header information as
      a comment starting with a '#' symbol.
    </example>

    <example>
      <title>Altering the coordinate order</title>

      <screen>
$ las2txt -i lidar.las -o lidar.txt -parse xyzcu -sep tab -header percent
      </screen>

      converts LAS file to ASCII and places the x, y, and z coordinate
      at the 1st, 2nd, and 3rd entry, the classification at the 4th and
      the user data as the 5th entry of each line.
      The entries are separated by a semicolon.
      At the beginning of the file we print the header information as
      a comment starting with a '%' symbol.
    </example> 

  </refsect1>

</refentry>
